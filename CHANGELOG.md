# Changelog

All notable changes to this project will be documented in this file.

<!-- Format:
## [0.0.1] - 2019-01-01 - Release description

### Added

### Changed

### Improved

### Removed

### Fixed
-->

## [0.1.1] - 2020-09-18

### Fixed

- Fixed ignore list duplicates

### Improved

- Small code rework for better readability

## [0.1.0] - 2020-09-17 - Initial release
