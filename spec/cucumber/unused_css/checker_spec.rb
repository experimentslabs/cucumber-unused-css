# frozen_string_literal: true

require 'spec_helper'
require 'nokogiri'
require 'capybara'

RSpec.describe Cucumber::UnusedCss::Checker do
  let(:fixture_file) { Cucumber::UnusedCss.root.join('spec', 'fixtures', 'sample.css').to_s }

  before do
    described_class.prepare output_directory: ''
    described_class.send :load_selectors, fixture_file
  end

  describe '#load_selectors' do
    it 'loads selectors' do
      expect(described_class.unused_selectors.count).to eq 4
    end

    it 'ignores specified selectors' do
      expect(described_class.ignored_selectors.count).to eq 1
    end
  end

  describe '#check_selectors' do
    let(:html) { Capybara::Node::Document.new 'fake', 'fake' }

    before do
      allow(html).to receive(:has_css?).with('body', wait: 0).and_return true
      allow(html).to receive(:has_css?).with('.title', wait: 0).and_return true
      allow(html).to receive(:has_css?).with('h1 .title', wait: 0).and_return true
      allow(html).to receive(:has_css?).with('p > a.description', wait: 0).and_return false
    end

    it 'finds unused selectors' do
      described_class.check_selectors html
      expect(described_class.unused_selectors.count).to eq 1
    end

    it 'finds used selectors' do
      described_class.check_selectors html
      expect(described_class.used_selectors.count).to eq 3
    end
  end
end
