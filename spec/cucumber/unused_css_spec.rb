# frozen_string_literal: true

RSpec.describe Cucumber::UnusedCss do
  it 'has a version number' do
    expect(Cucumber::UnusedCss::VERSION).not_to be nil
  end
end
