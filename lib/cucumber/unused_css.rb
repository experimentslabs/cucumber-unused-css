# frozen_string_literal: true

require 'cucumber/unused_css/version'
require 'cucumber/unused_css/checker'

module Cucumber
  module UnusedCss
    class Error < StandardError; end

    def self.root
      Pathname.new File.dirname(File.dirname(__dir__))
    end
  end
end
