# Cucumber::UnusedCss

This gem aims to help you find unused CSS in your projects by checking selectors of the visited page in Cucumber steps.

Currently, it has been tested on a Rails application, but _should_ work in other setups.

This project is young, MRs are welcome to polish and/or improve it.

Sample output:

```text
103 scenarios (3 failed, 100 passed)
660 steps (2 failed, 2 skipped, 656 passed)
13m30.584s

Randomized with seed 1861

CSS selectors summary
---------------------
 > Ignored selectors: 253
 > Used selectors: 239
 > Unused selectors: 238
 > Total selectors: 730

Lists saved in tmp as css_*.yml
```

## Notes on CI

Don't use this in CI: results depend on your Cucumber steps, so unless you wrote specific steps that covers all your use cases, you will get false positives. And it's slow (see "Benchmark")

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'cucumber-unused_css', required: false
```

And then execute:

```shell
bundle
```

## Cucumber configuration

We use two Cucumber hooks: `AfterStep` and `at_exit`. As the script really slows down the runs, you should not enable it by default. Edit your your preferred Cucumber configuration file and add:

```rb
require 'cucumber/unused_css'

# Collect stylesheets and search for unused styles in visited pages
if ENV['UNUSED_CSS'] == 'true'
  # Specify output for summary files
  Cucumber::UnusedCss::Checker.prepare output_directory: Rails.root.join('tmp')
  # Add regexes for custom selectors to ignore
  Cucumber::UnusedCss::Checker.ignore [
    # Libraries
    /^\.CodeMirror/,
    /^\.cm-/,
    /^\.hljs/,
  ]

  # Ran after each step
  AfterStep do
    # Collect CSS stylesheets and add selectors to the list
    Cucumber::UnusedCss::Checker.load_css_in_page html, Capybara.current_url
    # Check for unused selectors in the page (if the step visited one)
    Cucumber::UnusedCss::Checker.check_selectors page
  end

  # Ran at shutdown
  at_exit do
    # Save summary files and display a small text
    Cucumber::UnusedCss::Checker.save_and_summarize
  end
end
```

## Usage

Depending on the environment variable you used, simply launch:

```shell
UNUSED_CSS=true bundle exec cucumber
```

## Benchmark

- Normal run, headless: **3m17.307s**
- Search for unused CSS, headless: **20m10.783s**

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and merge requests are welcome on GitLab at https://gitlab.com/experimentslabs/cucumber-unused-css. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Cucumber::UnusedCss project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](CODE_OF_CONDUCT.md).
